import React, { Component } from 'react';
import Dired from './Dired';
import _ from 'lodash';

class App extends Component {
  state = {
    files: []
  }
  componentDidMount() {
    this.setupInput();
  }
  componentDidUpdate() {
    this.setupInput();
    console.log(this.state.files);
  }
  setupInput() {
    // necessary since react does not accept these 2 properties
    // on an input element, so we set them up after render
    this.refs.FileInput.directory = true;
    this.refs.FileInput.webkitdirectory = true;
  }
  // init app when files changes in the input
  init = (event) => {
    event.preventDefault();
    let files = this.getFiles(event.target.files)
    this.setState({files});
  }
  // fileArray to array of fileObjects
  getFiles(files) {
    let f = Array.from(files).map((file) => {
      file.folder = file.webkitRelativePath.replace(file.name, '');
      return file;
    });
    return this.groupFiles(f);
  }
  groupFiles(files) {    
    let groupObject =  _.groupBy(files, "folder");
    return Object.keys(groupObject).map((key) => {
      return groupObject[key];
    });
  }
  render() {
    return (
      <div className="App">
	<section>
	  <label>
	    <p>Select the folder to scan</p>
	    <input onChange={ this.init } type="file" ref="FileInput" multiple/>
	  </label>	  
	</section>
	<section>
	  <Dired files={ this.state.files }/>
	</section>	
      </div>
    );
  }
}

export default App;
