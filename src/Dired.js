import React, { Component } from 'react';

class Dired extends Component {
  list() {
    return this.props.files.map((files, index) => {
      return <li key={index}>{ files[0].folder }</li>
    })
  }
  render() {
    return (
      <div className="Dired">
	<p>Generated list:</p>
	<ul>
	  { this.list() }
	</ul>
      </div>
    )
  }

}

export default Dired;
